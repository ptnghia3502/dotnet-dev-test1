﻿using System.Collections.Generic;

namespace SortArrayDESC;

public class Program
{
    static void Main(string[] args)
    {
        var sorted = SortArrayDesc(new int[] { 1, 6, -2, 9, 10, 4, 1 });
        string result = string.Join(" ", sorted);
        Console.WriteLine(result);
    }

    public static int[] SortArrayDesc(int[] x)
    {
        // bubble sort
        for (int i = x.Length - 1; i >= 1; i--)
        {
            bool swapped = true;
            for (int j = 0; j < i; j++)
            {
                if (x[j] < x[j + 1])
                {
                    int temp = x[j];
                    x[j] = x[j + 1];
                    x[j + 1] = temp;
                    swapped = false;
                }
            }
            if (swapped)
            {
                break;
            }
        }
        return x;

    }

}