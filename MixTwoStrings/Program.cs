﻿using System.Text;

var result = PracticeStrings.MixTwoStrings("ccc", "dddeee");
Console.WriteLine(result);
Console.WriteLine("hello world");

public class PracticeStrings
{

    public static string MixTwoStrings(string value1, string value2)
    {
        // xem file README.md
        StringBuilder sb = new StringBuilder();
        string longerString;
        // tìm chuỗi dài nhất
        if (value1.Length >= value2.Length)
        {
            longerString = value1;
        }
        else
        {
            longerString = value2;
        }
        // nối chuỗi 
        for (int i = 0; i < longerString.Length; i++)
        {
            if (i < value1.Length)
            {
                sb.Append(value1[i]);
            }
            if (i < value2.Length)
            {
                sb.Append(value2[i]);
            }
        }
        return sb.ToString();

    }
}

