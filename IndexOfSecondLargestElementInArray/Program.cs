﻿using System;
using System.Reflection;

namespace IndexOfSecondLargestElementInArray;

public class Program
{
    static void Main(string[] args)
    {
        var myArr = new[] { -1, 2, -5 };
        var result = IndexOfSecondLargestElementInArray(myArr);
        Console.WriteLine(result);
    }

    public static int IndexOfSecondLargestElementInArray(int[] x)
    {
        // kết quả hàm sau khi thực thi là vị trí(index) của số lớn thứ 2 trong mảng
        if (x.Length == 0 || x.Length == 1)
        {
            return -1;
        }
        else
        {
            int maxNumber = 0;
            int maxNumberNewArray = 0;
            int IndexOfFirstLargestElementArray = 0;
            int IndexOfSecondLargestElementArray = 0;
            // tìm số lớn nhất của mảng
            for (int i = 0; i < x.Length; i++)
            {
                maxNumber = Math.Max(maxNumber, x[i]);
            }
            // lấy index của số lớn nhất
            for (int j = 0; j < x.Length; j++)
            {
                if (x[j] == maxNumber)
                {
                    IndexOfFirstLargestElementArray = j;
                    break;
                }
            }

            // remove số lớn nhất khỏi mảng theo index 
            for (int k = IndexOfFirstLargestElementArray; k < x.Length - 1; k++)
            {
                x[k] = x[k + 1];
            }
            // duyệt lại mảng mới để tìm số lớn nhì, vẫn có thể trùng với số lớn nhất
            for (int m = 0; m < x.Length - 1; m++)
            {
                maxNumberNewArray = Math.Max(maxNumberNewArray, x[m]);
            }
            // lấy index số lớn nhì
            for (int n = 0; n < x.Length - 1; n++)
            {
                if (x[n] == maxNumberNewArray)
                {
                    IndexOfSecondLargestElementArray = n;
                }
            }
            //if (maxNumberNewArray == maxNumber) return IndexOfSecondLargestElementArray;
            return IndexOfSecondLargestElementArray;
        }
    }
}
